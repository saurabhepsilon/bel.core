﻿namespace bel.core.Orders.CustomOrder
{
    public class DesignLine
    {
        public string Text { get; set; }

        public string FontName { get; set; }

        public string FontSize { get; set; }
    }
}
