﻿using System.Collections.Generic;

namespace bel.core.Orders.CustomOrder
{
    public class DesignItem
    {
        public IEnumerable<DesignLine> DesignLines { get; set; }

        public DesignItem()
        {
            DesignLines = new List<DesignLine>();
        }
    }
}
